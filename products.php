<?php
    include "db.php";

    /*
        Class with static methods for product manipulation
     */
    class ProductManip
    {
        static function importProducts()        // method to import all products and show them as cards in the list
        {
            $importStr = "";
            $conn = openConn();     // open connection to database

            $products = $conn->query("SELECT * FROM products");
            while($product = $products->fetch_assoc())      // takes every product from table with products
            {
                $productObj = new $product['class']();      // creates an instance of a specific class depending on the product type
                foreach ($product as $prop => $value)       // filling in all product properties
                {
                    $productObj->__set($prop, $value);
                }
                $importStr .= $productObj->createCard();    // creates card with product info to show it on the list page
            }

            closeConn($conn);     // close connection to database
            return $importStr;
        }

        static function deleteProduct($checkedProducts)     // method that delete all checked product cards
        {
            $conn = openConn();

            if (isset($checkedProducts["prodCard"]))        // checks if any card is selected
            {
                $toDelete = implode(",", $checkedProducts["prodCard"]);
                $conn->query("DELETE FROM products WHERE sku IN $toDelete");   // deletes products from products table
            }

            closeConn($conn);
        }

        static function addProduct($values)         // assembles product and adds it to the database
        {
            $product = new $values['type']();       // creates specific class instance, that reprezents product depending on its type
            foreach ($values as $prop => $value)    // fills in all product properties
            {
                $product->__set($prop, $value);
            }
            $product->addToDataBase();              // adds product to products table inside database
        }
    }


    /*
        Abstract class for product basic attributes,
        such as SKU, name and price and
        method to create a card for list page
    */
    abstract class Product
    {
        protected $sku;
        protected $name;
        protected $price;

        function __get($prop)
        {
            if (property_exists($this, $prop))
            {
                return $this->$prop;
            }
        }

        function __set($prop, $value)
        {
            if (property_exists($this, $prop) && !is_null($value))
            {
                $this->$prop = $value;
            }
        }

        abstract function createCard();
        abstract function addToDataBase();
    }

    /*
        Class for dvd product type with special attribute - size
    */
    class DVDProduct extends Product
    {
        protected $size;

        function createCard()
        {
            return
            ("<div class=\"card\">
                <input type=\"checkbox\" name=\"prodCard[]\" value=\"'$this->sku'\"/>
                <div class=\"product_info\">
                    <p>$this->sku</p>
                    <p>$this->name</p>
                    <p>" . number_format($this->price, 2) . " $</p>
                    <p>Size: $this->size MB</p>
                </div>
            </div>");
        }

        function addToDataBase()
        {
            $conn = openConn();
            $conn->query("INSERT INTO products(sku, name, price, class, size) VALUES('$this->sku', '$this->name', $this->price, 'DVDProduct', $this->size)");
            closeConn($conn);
        }
    }

    /*
        Class for book product type with special attribute - weight
    */
    class BookProduct extends Product
    {
        protected $weight;

        function createCard()
        {
            return
            ("<div class=\"card\">
                <input type=\"checkbox\" name=\"prodCard[]\" value=\"'$this->sku'\"/>
                <div class=\"product_info\">
                    <p>$this->sku</p>
                    <p>$this->name</p>
                    <p>" . number_format($this->price, 2) . " $</p>
                    <p>Weight: $this->weight" . "KG</p>
                </div>
            </div>");
        }

        function addToDataBase()
        {
            $conn = openConn();
            $conn->query("INSERT INTO products(sku, name, price, class, weight) VALUES('$this->sku', '$this->name', $this->price, 'BookProduct', $this->weight)");
            closeConn($conn);
        }
    }

    /*
        Class for furniture product type with special attributes - height, width, length
    */
    class FurnitureProduct extends Product
    {
        protected $height;
        protected $width;
        protected $length;

        function createCard()
        {
            return
            ("<div class=\"card\">
                <input type=\"checkbox\" name=\"prodCard[]\" value=\"'$this->sku'\"/>
                <div class=\"product_info\">
                    <p>$this->sku</p>
                    <p>$this->name</p>
                    <p>" . number_format($this->price, 2) . " $</p>
                    <p>Dimension: $this->height" . "x" . $this->width . "x" . "$this->length</p>
                </div>
            </div>");
        }

        function addToDataBase()
        {
            $conn = openConn();
            $conn->query("INSERT INTO products(sku, name, price, class, height, width, length) VALUES('$this->sku', '$this->name', $this->price, 'FurnitureProduct', $this->height, $this->width, $this->length)");
            closeConn($conn);
        }
    }
?>
