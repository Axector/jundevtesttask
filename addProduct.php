<?php
    include "products.php";
    include "validator.php";

////////////////// VALIDATION //////////////////

    if (isset($_POST["submit"]))        // checks if submit button was pressed
    {                                   // saving user-entered data into variables
        $sku = $_POST["sku"];
        $name = $_POST["name"];
        $price = $_POST["price"];
        $type = $_POST["type"];
        $size = (isset($_POST["size"])) ? $_POST["size"] : NULL;            // these are special atributes,
        $weight = (isset($_POST["weight"])) ? $_POST["weight"] : NULL;      // so at first I should check which type is selected
        $height = (isset($_POST["height"])) ? $_POST["height"] : NULL;
        $width = (isset($_POST["width"])) ? $_POST["width"] : NULL;
        $length = (isset($_POST["length"])) ? $_POST["length"] : NULL;

        // validation starts with creation of variables
        // that I need to check user input correctness
        $emptyError = $dataError = $errorSku = $errorName = $errorPrice = $errorType = false;
        $uniqueError = $errorSize = $errorWeight = $errorHeight = $errorWidth = $errorLength = false;

        Validator::validateSku($sku, $errorSku, $emptyError, $dataError, $uniqueError);
        Validator::validateName($name, $errorName, $emptyError, $dataError);
        Validator::validatePrice($price, $errorPrice, $emptyError, $dataError);
        Validator::validateType($type, "Type Switcher", $errorType, $emptyError);
        Validator::validateSize($size, $errorSize, $emptyError, $dataError);
        Validator::validateWeight($weight, $errorWeight, $emptyError, $dataError);
        Validator::validateHeight($height, $errorHeight, $emptyError, $dataError);
        Validator::validateWidth($width, $errorWidth, $emptyError, $dataError);
        Validator::validateLength($length, $errorLength, $emptyError, $dataError);

        // Shows error messages if there is any incorrect input
        // 3 categories: empty input field, incorrect input data
        // and if entered SKU already exsists
        if($emptyError) echo '<p>"Please, submit required data"</p>';
        if($dataError) echo '<p>"Please, provide the data of indicated type"</p>';
        if($uniqueError) echo '<p>"Product with this SKU already exists"</p>';
    }
    else
    // Error if something went wrong pressing submit button
    {
        echo "<p>There was an error in submission</p>";
    }
?>

<script>
    $(".validating").removeClass("error_input");

    // marks input fields that have an error inside
    if("<?php echo $errorSku; ?>"){
        $("#product_sku").addClass("error_input");
    }
    if("<?php echo $errorName; ?>"){
        $("#product_name").addClass("error_input");
    }
    if("<?php echo $errorPrice; ?>"){
        $("#product_price").addClass("error_input");
    }
    if("<?php echo $errorType; ?>"){
        $("#product_type").addClass("error_input");
    }
    if("<?php echo $errorSize; ?>"){
        $("#dvd_size").addClass("error_input");
    }
    if("<?php echo $errorWeight; ?>"){
        $("#book_weight").addClass("error_input");
    }
    if("<?php echo $errorHeight; ?>"){
        $("#furniture_height").addClass("error_input");
    }
    if("<?php echo $errorWidth; ?>"){
        $("#furniture_width").addClass("error_input");
    }
    if("<?php echo $errorLength; ?>"){
        $("#furniture_length").addClass("error_input");
    }
</script>


<?php
/////////////////// PRODUCT ADDITION ///////////////////
    if(!$emptyError && !$dataError && !$uniqueError)
    // if there is not any validation error we can add new product
    {
        // Gets url of the page to redirect to differnet page at the end of this block
        $url = (((!empty($_SERVER['HTTPS']) && $_SERVER['HTTPS'] != 'off') || $_SERVER['SERVER_PORT'] == 443) ? "https://" : "http://") . $_SERVER['HTTP_HOST'];

        ProductManip::addProduct(
            array('sku' => $sku,
            'name' => $name,
            'price' => $price,
            'type' => $type,
            'size' => $size,
            'weight' => $weight,
            'height' => $height,
            'width' => $width,
            'length' => $length)
        );
?>
        <script>    // redirecting to list page
            $(location).prop('href', '<?php echo "$url/$defaultStartPage"; ?>');
        </script>
<?php
    }
?>
