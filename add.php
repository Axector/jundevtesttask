<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <title>Product Add</title>
        <link rel="stylesheet" href="style.css"/>
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
        <script>
            $(document).ready(function() {
                $("form").submit(function(e){       // Submission with validation
                    e.preventDefault();             // Turning off default post method
                    $(".errors").load("addProduct.php", {
                        submit: $("#submit").val(),
                        sku: $("#product_sku").val(),
                        name: $("#product_name").val(),
                        price: $("#product_price").val(),
                        type: $("#product_type").val(),
                        size: $("#dvd_size").val(),
                        weight: $("#book_weight").val(),
                        height: $("#furniture_height").val(),
                        width: $("#furniture_width").val(),
                        length: $("#furniture_length").val()
                    });
                });

                let special = {     // json with input fields for different product types
                                "DVDProduct": '<div class="product_dvd_special hideSpecial"><div><label for="dvd_size">Size (MB)</label><input type="text" class="validating" id="dvd_size" name="dvd_size"></div></div>',
                                "BookProduct": '<div class="product_book_special hideSpecial"><div><label for="book_weight">Weight (KG)</label><input type="text" class="validating" id="book_weight" name="book_weight"></div></div>',
                                "FurnitureProduct": '<div class="product_furniture_special hideSpecial"><div><label for="furniture_height">Height (CM)</label><input type="text" class="validating" id="furniture_height" name="furniture_height"></div><div><label for="furniture_width">Width (CM)</label><input type="text" class="validating" id="furniture_width" name="furniture_width"></div><div><label for="furniture_length">Length (CM)</label><input type="text" class="validating" id="furniture_length" name="furniture_length"></div></div>'
                              };

                $("#product_type").change(function(e){      // Dynamic input fields' change according to selected product type
                    $(".product_special_description").text('"Please, provide ' + e.target.value + '"');
                    $("#product_special").html(special[e.target.value]);
                });
            });
        </script>
        <style>
            #add{
                padding: 3em;
            }

            button.submit_button{
                height: 1.78em;
                font-size: 1em;
                cursor: pointer;
                padding: 3px 8px;
                margin-right: 2em;
                font-weight: bold;
                box-shadow: 3px 3px;
                background-color:white;
                border: solid 2px black;
            }

            #add .errors p{
                margin-bottom: 1em;
                color: red;
            }

            #add input, #add select{
                border: solid 2px black;
                border-radius: 2px;
            }

            #add .basic_info, #add #product_special{
                width: 17em;
                line-height: 1.5em;
            }

            #add .basic_info div, #add #product_special > div > div{
                display: flex;
                margin-bottom: 1.5em;
                justify-content: space-between;
            }

            #add .type_select{
                margin-bottom: 1.5em;
            }

            #add .type_select label{
                margin-right: 1em;
            }

            #add .product_special_description{
                font-weight: normal;
                margin-bottom: 1.5em;
            }

            .hidden{
                display: none;
            }

            .error_input{
                box-shadow: 0 0 5px red;
            }
        </style>
    </head>
    <body>
        <form action="addProduct.php" method="post">
        <header>
            <h1>Product Add</h1>
            <div class="header-buttons">
                <button type="submit" id="submit" name="submit" class="header-button-margin submit_button">Save</button>
                <a href="list.php" class="header-button-margin"><div class="header-button-style">Cancel</div></a>
            </div>
        </header>
        <hr>

            <section id="add">
                <div class="basic_info">
                    <div class="sku_select">
                        <label for="product_sku">SKU</label>
                        <input type="text" class="validating" id="product_sku" name="product_sku">
                    </div>
                    <div class="name_select">
                        <label for="product_name">Name</label>
                        <input type="text" class="validating" id="product_name" name="product_name">
                    </div>
                    <div class="price_select">
                        <label for="product_price">Price ($)</label>
                        <input type="text" class="validating"  id="product_price" name="product_price">
                    </div>
                </div>
                <div class="type_select">
                    <label for="product_type">Type Switcher</label>
                    <select id="product_type" class="validating" name="product_type">
                        <option class="hidden">Type Switcher</option>
                        <option value="DVDProduct">DVD</option>
                        <option value="BookProduct">Book</option>
                        <option value="FurnitureProduct">Furniture</option>
                    </select>
                </div>

                <div id="product_special"></div>

                <p class="product_special_description"></p>
                <div class="errors"></div>
            </section>
        </form>
        <hr>
        <footer>
            <p>Scandiweb Test assignment</p>
        </footer>
    </body>
</html>
