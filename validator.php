<?php

    /*
        Class for input fields' validation
     */
    class Validator
    {
        static function validateSku($sku, &$errorSku, &$emptyError, &$dataError, &$uniqueError)
        {
            if(self::isEmpty($sku))        // checks if SKU input field is empty
            {
                $errorSku = $emptyError = true;
            }
            else{
                if(self::notOnlyNumbersAndLetters($sku, 20))
                // checks if entered SKU consists from letters or numbers, also max length is 20 symbols
                {
                    $errorSku = $dataError = true;
                }
                else
                {
                    $sku = strtoupper($sku);
                    // checks if entered SKU already exsists in database
                    if(self::notUnique($sku)) $errorSku = $uniqueError = true;
                }
            }
        }

        static function validateName($name, &$errorName, &$emptyError, &$dataError)
        {
            if(self::isEmpty($name))       // checks if name input field is empty
            {
                $errorName = $emptyError = true;
            }
            else
            {
                // checks if entered name consists from letters, numbers or spaces, also max length is 30 symbols
                if(self::notOnlyNumbersAndLettersWithSpace($name, 30)) $errorName = $dataError = true;
            }
        }

        static function validatePrice($price, &$errorPrice, &$emptyError, &$dataError)
        {
            if(self::isEmpty($price))      // checks if price input field is empty
            {
                $errorPrice = $emptyError = true;
            }
            else
            {
                // checks if entered price is an integer or a number with floating point
                if(self::notFloat($price)) $errorPrice = $dataError = true;
            }
        }

        static function validateType($type, $notToBeSelected, &$errorType, &$emptyError)
        {
            if($type == $notToBeSelected)    // checks if user selected any product type
            {
                $errorType = $emptyError = true;
            }
        }

        static function validateSize($size, &$errorSize, &$emptyError, &$dataError)
        {
            if(!is_null($size))
            // checks if size is not null, which means that dvd product type not selected
            {
                if (self::isEmpty($size))      // checks if size input field is empty
                {
                    $errorSize = $emptyError = true;
                }
                else
                {
                    // checks if entered size is an integer
                    if(self::notInteger($size)) $errorSize = $dataError = true;
                }
            }
        }

        static function validateWeight($weight, &$errorWeight, &$emptyError, &$dataError)
        {
            if(!is_null($weight))
            // checks if weight is not null, which means that book product type not selected
            {
                if (self::isEmpty($weight))    // checks if weight input field is empty
                {
                    $errorWeight = $emptyError = true;
                }
                else
                {
                        // checks if entered weight is an integer
                        if(self::notInteger($weight)) $errorWeight = $dataError = true;
                }
            }
        }

        static function validateHeight($height, &$errorHeight, &$emptyError, &$dataError)
        {
            if(!is_null($height))
            // checks if height is not null, which means that furniture product type not selected
            {
                if (self::isEmpty($height))    // checks if height input field is empty
                {
                    $errorHeight = $emptyError = true;
                }
                else
                {
                    // checks if entered height is an integer
                    if(self::notInteger($height)) $errorHeight = $dataError = true;
                }
            }
        }

        static function validateWidth($width, &$errorWidth, &$emptyError, &$dataError)
        {
            if(!is_null($width))
            // checks if width is not null, which means that furniture product type not selected
            {
                if (self::isEmpty($width))     // checks if width input field is empty
                {
                    $errorWidth = $emptyError = true;
                }
                else{
                    // checks if entered width is an integer
                    if(self::notInteger($width)) $errorWidth = $dataError = true;
                }
            }
        }

        static function validateLength($length, &$errorLength, &$emptyError, &$dataError)
        {
            if(!is_null($length))
            // checks if length is not null, which means that furniture product type not selected
            {
                if (self::isEmpty($length))    // checks if length input field is empty
                {
                    $errorLength = $emptyError = true;
                }
                else
                {
                    // checks if entered length is an integer
                    if(self::notInteger($length)) $errorLength = $dataError = true;
                }
            }
        }

        static function notOnlyNumbersAndLetters($value, $maxLength)
        {
            return !preg_match("/^[a-zA-Z0-9]{1,$maxLength}$/", $value);
        }

        static function notOnlyNumbersAndLettersWithSpace($value, $maxLength)
        {
            return !preg_match("/^[a-zA-Z0-9 ]{1,$maxLength}$/", $value);
        }

        static function notUnique($value)
        {
            $conn = openConn();
            if($conn->query("SELECT sku FROM products WHERE sku = '$value'")->num_rows)
            {
                closeConn($conn);
                return true;
            }
            else
            {
                closeConn($conn);
                return false;
            }
        }

        static function isEmpty($value)
        {
            return empty($value);
        }

        static function notFloat($value)
        {
            return !preg_match("/^(?:[1-9][0-9]*|[0-9][0-9]*\.[0-9]+)$/", $value);
        }

        static function notInteger($value)
        {
            return !preg_match("/^[1-9][0-9]*$/", $value);
        }
    }

?>
