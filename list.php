<?php require "products.php"; ?>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <title>Product List</title>
        <link rel="stylesheet" href="style.css"/>
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
        <script>
            $(document).ready(function() {
                $("#delete_cards_submit").on("click", function() {
                    $("#delete_cards").submit();
                });
            });
        </script>
        <style>
            #list{
                display: flex;
                flex-flow: wrap;
                font-weight: normal;
                padding: 1em 0 2em 0;
                justify-content: center;
            }

            #list div.card{
                width: 12em;
                height: 8em;
                padding: 1em;
                margin: 1.5em;
                border: solid 2px black;
            }

            #list div.product_info{
                text-align: center;
            }
        </style>
    </head>
    <body>
        <header>
            <h1>Product List</h1>
            <div class="header-buttons">
                <a href="add.php" class="header-button-margin"><div class="header-button-style">ADD</div></a>
                <a id="delete_cards_submit" class="header-button-margin"><div class="header-button-style">MASS DELETE</div></a>
            </div>
        </header>
        <hr>

        <form action="deleteProduct.php" id="delete_cards" method="post">
            <section id="list">
                <?php
                    echo ProductManip::importProducts();     // Call method that returns assembled cards with products' info from database
                ?>
            </section>
        </form>

        <hr>
        <footer>
            <p>Scandiweb Test assignment</p>
        </footer>
    </body>
</html>
