<?php
    include "products.php";

    $url = (((!empty($_SERVER['HTTPS']) && $_SERVER['HTTPS'] != 'off') || $_SERVER['SERVER_PORT'] == 443) ? "https://" : "http://") . $_SERVER['HTTP_HOST'];
    // Gets url of the page to redirect to differnet page at the end of this block

    ProductManip::deleteProduct($_POST);

    header("Location: $url/$defaultStartPage"); // redirect to start page location
    exit();
?>
